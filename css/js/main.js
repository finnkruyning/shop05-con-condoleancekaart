function placeHolder(element){
	var standard_message = $(element).val();
	$(element).focus(
		function() {
			if ($(this).val() == standard_message)
				$(this).val("");
		}
	);
	$(element).blur(
		function() {
			if ($(this).val() == "")
				$(this).val(standard_message);
		}
	);
}
jQuery(document).ready(function(){
	jQuery("#mid").wrapInner("<div id='wrap-inner'>");
	jQuery("#bxsliders").remove().insertBefore($("#wrap-inner"));
	jQuery(".tophead").remove().insertBefore($("#wrapper"));
	jQuery(".flogo").remove().insertAfter($("#wrapper"));
	
	
	$('#menu li').each(function(){
		var hre_menu = $(this).find('a').attr('href');	
		var path = window.location.pathname;
		
		var path_arr = path.split("/");
		var hre_menu_arr = hre_menu.split("/");
		
		if (path_arr[1] == hre_menu_arr[1]) {
			$(this).addClass('active');
		}
		if (path_arr[1] == 'condoleancekaarten') {
			if (hre_menu_arr[1] == 'condoleancekaarten') {
				$(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'condoleance_gedichten') {
			if (hre_menu_arr[1] == 'condoleance_gedichten') {
				$(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'condoleance_teksten') {
			if (hre_menu_arr[1] == 'condoleance_teksten') {
				$(this).addClass('active');
			}
		}
		
		if (path_arr[1] == 'contact') {
			if (hre_menu_arr[1] == 'contact') {
				$(this).addClass('active');
			}
		}
		
		
	});
	
	var croll = $(window).scrollTop();
	if (croll > 10) {
		$('.save-date').addClass('showlogo');
		$('.save-date').parents('div.tophead').addClass('bor');
		$('.save-date .logosmall').show();
	} else {
		$('.save-date').parents('div.tophead').removeClass('bor');
		$('.save-date .logosmall').fadeOut();
		$('.save-date .logosmall').hide();
		$('#home').show();
	}
	
	$(window).scroll(function(){
		var croll = $(window).scrollTop();
		if (croll > 10) {
			$('.save-date').parents('div.tophead').addClass('bor');
			$('.save-date').addClass('showlogo');
			$('.save-date .logosmall').fadeIn();
		} else {
			$('.save-date').parents('div.tophead').removeClass('bor');
			$('.save-date').removeClass('showlogo');
			$('.save-date .logosmall').fadeOut();
		}
	});
	
	$('#card_previews_horizontal #inner .selected').append("<span></span>"); 
	$('#knav').append("<span class='mask-left'></span><span class='mask-right'></span>"); 
	if ($("#mainmenu").length >0) {
		var path = window.location.pathname;
		$("#mainmenu li ul li").each(function(){
			var hre_menu = $(this).find('a').attr('href');
			if (path == hre_menu) {
				$(this).addClass("active");
			}
		});
	}
}); 